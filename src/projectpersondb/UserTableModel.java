/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectpersondb;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author O-Live
 */
public class UserTableModel extends AbstractTableModel {

    String[] columName = {"Id", "Username", "First-Name", "Last-Name"};
    ArrayList<User> userList = Data.userList;

    @Override
    public String getColumnName(int colum) {
        return columName[colum];
    }

    @Override
    public int getRowCount() {
        return userList.size();

    }

    @Override
    public int getColumnCount() {
        return columName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columIndex) {
        User user = userList.get(rowIndex);
        if (user == null) {
            return "";

        }
        switch (columIndex) {
            case 0:
                return user.getId();
            case 1:
                return user.getUsername();
            case 2:
                return user.getFirstname();
            case 3:
                return user.getLastname();
        }

        return "";
    }

}
